# tooltime-polls

> tooltime polls

## Details

Project is using VueJS framework with bootstrap grid.
It was done on top of Vue Template:
  https://github.com/vuejs-templates/webpack-simple

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
