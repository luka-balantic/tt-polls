import Vue from 'vue';

import App from './App.vue';
import router from './router';
import dependencies from './dependencies';


new Vue({
  el: '#app',
  router,
  dependencies,
  render(createElement) {
    return createElement(App);
  },
});
