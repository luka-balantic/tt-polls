import Vue from 'vue';
import axios from 'axios'

const dependencies = {
  // constants
  basePollApiUrl: 'https://polls.apiblueprint.org',

  // libraries
  axios,
};

Vue.use({
  install() {
    // This will pass the injected dependency to all child components.
    Vue.mixin({
      beforeCreate() {
        const options = this.$options;

        if (options.dependencies) {
          this.$dependencies = options.dependencies;
        } else if (options.parent && options.parent.$dependencies) {
          this.$dependencies = options.parent.$dependencies;
        }
      },
    });
  },
});

export default dependencies;
