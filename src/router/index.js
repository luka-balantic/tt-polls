import Vue from 'vue';
import VueRouter from 'vue-router';

import QuestionList from '../pages/QuesitonList.vue';
import QuestionDetail from '../pages/QuestionDetail.vue';
import QuestionCreate from '../pages/QuestionCreate.vue';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      name: 'home',
      path: '/',
      component: QuestionList,
    },
    {
      name: 'question-details',
      path: '/questions/:id',
      component: QuestionDetail,
    },
    {
      name: 'question-create',
      path: '/question-create',
      component: QuestionCreate,
    },
  ]
});

export default router;
